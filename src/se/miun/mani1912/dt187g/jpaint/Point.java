package se.miun.mani1912.dt187g.jpaint;

/**
 * Create a Point. Points are used as starting points and ending points
 * for the Rectangle and Circle shapes.
 * 
 * @author mani1912
 * @version 1.0
 */

public class Point {
    private double x;
    private double y;

    public Point() {
        x = 0;
        y = 0;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
