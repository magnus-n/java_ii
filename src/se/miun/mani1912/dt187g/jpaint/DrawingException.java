package se.miun.mani1912.dt187g.jpaint;

public class DrawingException extends Exception {
    
    public DrawingException (String msg) {
        super(msg);
    }
}
