package se.miun.mani1912.dt187g.jpaint;

/**
 * Interface for the Shape class.
 * 
 * @author mani1912
 * @version 1.0
 */

public interface Drawable {
    void draw();

    void draw(java.awt.Graphics g);
}
