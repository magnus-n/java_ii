package se.miun.mani1912.dt187g.jpaint;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.WindowConstants;

import java.awt.event.*;

import java.awt.*;
import java.util.ArrayList;

public class JPaintFrame extends JFrame {
    private JMenuBar menuBar;
    private JMenu menu1;
    private JMenu menu2;
    private JMenuItem menuItem11;
    private JMenuItem menuItem12;
    private JMenuItem menuItem13;
    private JMenuItem menuItem14;
    private JMenuItem menuItem15;
    private JMenuItem menuItem21;
    private JMenuItem menuItem22;
    private JMenuItem menuItem23;

    private JPanel colorPanel;
    private JPanel colorPanelGreen;
    private JPanel colorPanelBlue;
    private JPanel colorPanelBlack;
    private JPanel colorPanelRed;
    private JPanel colorPanelYellow;
    private JComboBox<String> combo;

    private DrawingPanel drawingPanel;

    private JPanel statusBar;
    private JPanel coordinatesPanel;
    private JLabel coordinatesLabel;
    private JLabel coordinates;
    private JPanel selectedColorPanel;
    private JLabel selectedColorLabel;
    private JPanel selectedColor;

    private String name;
    private String author;
    
    
    public JPaintFrame() {

        setTitle("JPaint");

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setSize(new Dimension(800, 640));

        setLocationRelativeTo(null);

        setResizable(true);

        // Icons made by Freepik: https://www.freepik.com"
        ArrayList<Image> iconImages = new ArrayList<Image>();
		iconImages.add(new ImageIcon("icon16x16.png").getImage());
		iconImages.add(new ImageIcon("icon32x132.png").getImage());
		iconImages.add(new ImageIcon("icon64x64.png").getImage());
		setIconImages(iconImages);

        initComponents();

        pack();

        /*
        System.out.println(getWindowTitle("Mona Lisa", "Da Vinci"));
        System.out.println(getWindowTitle(null, "Da Vinci"));
        System.out.println(getWindowTitle("Mona Lisa", null));
        System.out.println(getWindowTitle(null, null));
        */

        /*
        System.out.println("Empty string " + formatString(""));
        System.out.println("Blank spaces " + formatString("    "));
        System.out.println("Trailing whitespaces " + formatString("   aaa   "));
        System.out.println("Normal string " + formatString("Hello"));
        */
    }

    private void initComponents() {

        BorderLayout layout = new BorderLayout();
        setLayout(layout);


        // Menu bar
        menuBar = new JMenuBar();
        menu1 = new JMenu("File");
        menu2 = new JMenu("Edit");
        menuBar.add(menu1);
        menuBar.add(menu2);

        menuItem11 = new JMenuItem("New...");
        menuItem12 = new JMenuItem("Save As...");
        menuItem13 = new JMenuItem("Load...");
        menuItem14 = new JMenuItem("Exit...");
        menuItem15 = new JMenuItem("Info...");
        menuItem11.addActionListener(new ActionListener(){
            // New...

            @Override
            public void actionPerformed(ActionEvent e) {
                String name = JOptionPane.showInputDialog(null, "Enter name of the drawing:");
                String author = JOptionPane.showInputDialog(null, "Enter author of the drawing:");
                Drawing drawing = new Drawing();
                drawing.setName(name);
                drawing.setAuthor(author);
                setWindowTitle(name, author);               
                drawingPanel.setDrawing(drawing);
            }

        });

        menuItem12.addActionListener(new ActionListener(){
            // Save As...

            @Override
            public void actionPerformed(ActionEvent e) {
                String fileName = JOptionPane.showInputDialog(null, "Save drawing to:", getWindowTitle() + ".shape");                           
            }

        });

        menuItem13.addActionListener(new ActionListener(){
            // Load...

            @Override
            public void actionPerformed(ActionEvent e) {
                String load = JOptionPane.showInputDialog(null, "Load drawing from:");
                Drawing drawing = drawingPanel.createTestDrawing();
                drawingPanel.setDrawing(drawing);
                
            }
            
        });

        menuItem14.addActionListener(new ActionListener(){
            // Exit..

            @Override
            public void actionPerformed(ActionEvent e) {
               System.exit(0); 
                
            }
            
        });

        menuItem15.addActionListener(new ActionListener(){
            // Info...

            @Override
            public void actionPerformed(ActionEvent e) {
                String infoString = "" + getWindowTitle() +
                                    "\nNumber of shapes: " + drawingPanel.getDrawing().shapes.size() +
                                    "\nTotal area: " + drawingPanel.getDrawing().getTotalArea() +
                                    "\nTotal circumference: " + drawingPanel.getDrawing().getTotalCircumference();
                 JOptionPane.showMessageDialog(null, infoString, "Info", JOptionPane.INFORMATION_MESSAGE);                          
            }

        });

        menu1.add(menuItem11);
        menu1.add(menuItem12);
        menu1.add(menuItem13);
        menu1.add(menuItem15);
        menu1.add(new JSeparator());
        menu1.add(menuItem14);

        menuItem21 = new JMenuItem("Undo...");
        menuItem22 = new JMenuItem("Name...");
        menuItem23 = new JMenuItem("Author...");

        menuItem21.addActionListener(new ActionListener(){
            // Undo...

            @Override
            public void actionPerformed(ActionEvent e) {
                int numberOfShapes = drawingPanel.getDrawing().shapes.size();
                if (numberOfShapes > 0 ) {
                    drawingPanel.getDrawing().shapes.remove(numberOfShapes - 1);;
                }         
            }

        });

        menuItem22.addActionListener(new ActionListener() {
            // Name...

            @Override
            public void actionPerformed(ActionEvent e) {
                String name = JOptionPane.showInputDialog(null, "Enter name of the drawing:");
                drawingPanel.getDrawing().setName(name);
                setWindowTitle(name, null);               
            }  

        });

        menuItem23.addActionListener(new ActionListener(){
            // Author...

            @Override
            public void actionPerformed(ActionEvent e) {
                String author = JOptionPane.showInputDialog(null, "Enter author of the drawing:");
                drawingPanel.getDrawing().setAuthor(author);
                setWindowTitle(null, author);               
            }
            
        });

        menu2.add(menuItem21);
        menu2.add(menuItem22);
        menu2.add(menuItem23);

        setJMenuBar(menuBar);

        // Color panel
        colorPanel = new JPanel();
        colorPanel.setLayout(new BoxLayout(colorPanel, BoxLayout.X_AXIS));

        colorPanelGreen = new JPanel();
        colorPanelBlue = new JPanel();;
        colorPanelBlack = new JPanel();
        colorPanelRed = new JPanel();
        colorPanelYellow = new JPanel();

        colorPanelGreen.setBackground(Color.decode("#00FF00"));  // Green
        colorPanelBlue.setBackground(Color.decode("#0000FF"));   // Blue
        colorPanelBlack.setBackground(Color.decode("#000000"));  // Black
        colorPanelRed.setBackground(Color.decode("#FF0000"));    // Red
        colorPanelYellow.setBackground(Color.decode("#FFFF00")); // Yellow

        colorPanelGreen.addMouseListener(new MouseAdapter() {
            @Override
			public void mouseClicked(MouseEvent e) {
				selectColor(e);
			}
        });

        colorPanelBlue.addMouseListener(new MouseAdapter() {
            @Override
			public void mouseClicked(MouseEvent e) {
				selectColor(e);
			}
        });

        colorPanelBlack.addMouseListener(new MouseAdapter() {
            @Override
			public void mouseClicked(MouseEvent e) {
				selectColor(e);
			}
        });

        colorPanelRed.addMouseListener(new MouseAdapter() {
            @Override
			public void mouseClicked(MouseEvent e) {
				selectColor(e);
			}
        });

        colorPanelYellow.addMouseListener(new MouseAdapter() {
            @Override
			public void mouseClicked(MouseEvent e) {
				selectColor(e);
			}
        });

        colorPanel.add(colorPanelGreen);
        colorPanel.add(colorPanelBlue);
        colorPanel.add(colorPanelBlack);
        colorPanel.add(colorPanelRed);
        colorPanel.add(colorPanelYellow);

        combo = new JComboBox<String>();
        combo.addItem("Rectangle");
        combo.addItem("Circle");
        combo.setPreferredSize(new Dimension(75, 30));
        combo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(e.getSource() + " selected");             
            }
            
        });
        colorPanel.add(combo);      

        add(colorPanel, BorderLayout.PAGE_START); 


        // DrawingPanel
        //DrawingPanel drawingPanel = new DrawingPanel();
        DrawingPanel drawingPanel = new DrawingPanel(new Drawing("Mona Lisa", "Da Vinci"));
        this.drawingPanel = drawingPanel;
        setWindowTitle(drawingPanel.getDrawing().getName(), drawingPanel.getDrawing().getAuthor());
        drawingPanel.setBackground(Color.WHITE);
        drawingPanel.setPreferredSize(new Dimension(800, 400));

        drawingPanel.addMouseMotionListener(new MouseAdapter() {
            @Override
			public void mouseMoved(MouseEvent e) {
				coordinates.setText(e.getX() + ", " + e.getY());
			}
        });

        add(drawingPanel, BorderLayout.CENTER);


        // Status bar
        statusBar = new JPanel(new BorderLayout());
        coordinatesPanel = new JPanel();
        coordinatesLabel = new JLabel("Coordinates: ");
        coordinates = new JLabel("127, 103");
        selectedColorPanel = new JPanel();
        selectedColorLabel = new JLabel("Selected color: ");
        selectedColor = new JPanel();

        selectedColor.setBackground(Color.GREEN);

        coordinatesPanel.add(coordinatesLabel);
        coordinatesPanel.add(coordinates);
        selectedColorPanel.add(selectedColorLabel);
        selectedColorPanel.add(selectedColor);
        statusBar.add(coordinatesPanel, BorderLayout.LINE_START);
        statusBar.add(selectedColorPanel, BorderLayout.LINE_END);
        add(statusBar, BorderLayout.PAGE_END);
        
    }

    private String getWindowTitle() {

        if((name == null || name == "") && author != null) {
            return author;
        }
        else if((author == null || author == "") && name != null) {
            return name;
        }
        else if (name == null && author == null) {
            return "";
        }
        else {
            return name + " by " + author;
        }
    }

    public void setWindowTitle(String name, String author) {

        if (name != null) {
            this.name = formatInputString(name);
        }

        if (author != null) {
            this.author = formatInputString(author);
        }
           
        setTitle(null);
        if (getWindowTitle() == "") {
            setTitle("JPaint");
        }
        else {
            setTitle("JPaint - " + getWindowTitle());
        }
        //setTitle("JPaint - " + getWindowTitle());              
        
    }

    private String formatInputString(String s) {
        // Remove strings with only whitespaces and trailing whitespaces

        if (s.isBlank()) {
            return "";
        }
        else {
            return s.trim();
        }
    }
    
    protected void selectColor(MouseEvent e) {
        selectedColor.setBackground(e.getComponent().getBackground());
    }

    protected String getFileName() {
        return getWindowTitle();
    }

}
