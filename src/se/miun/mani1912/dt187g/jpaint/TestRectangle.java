package se.miun.mani1912.dt187g.jpaint;

/**
 * Test class for Rectangle. Create a new Rectangle, add the endpoint and prints
 * the toString.
 * 
 * @author mani1912
 * @version 1.0
 */

public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(3, 4, "red");
        rectangle.addPoint(5, 6);
        System.out.println(rectangle.toString());
    }

}
