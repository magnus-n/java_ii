package se.miun.mani1912.dt187g.jpaint;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

/**
 * This class holds the different shapes drawn on the canvas, as well as the
 * name of the drawing and it's author. There are getters and setters for the
 * name and author fields, and methods to add shapes to the list and get the
 * size of the list. There's also methods to get the total area and total
 * circumference of all of the sahapes on the drawing.
 * 
 * @author mani1912
 * @version 1.0
 */

public class Drawing implements Drawable {
    private String name;
    private String author;
    protected List<Shape> shapes;

    public Drawing() {
        shapes = new ArrayList<Shape>();
    }

    public Drawing(String name, String author) {
        this.name = name;
        this.author = author;
        shapes = new ArrayList<Shape>();
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean addShape(Shape shape) {
        if (shape == null) {
            return false;
        } else {
            shapes.add(shape);
            return true;
        }
    }

    public int getSize() {
        return shapes.size();
    }

    public double getTotalCircumference() {
        double sum = 0;
        double circumference;
        for (Shape s : shapes) {
            circumference = s.getCircumference();
            if (circumference != -1) {
                sum += circumference;
            }
        }
        return sum;
    }

    public double getTotalArea() {
        double sum = 0;
        double area;
        for (Shape s : shapes) {
            area = s.getArea();
            if (area != -1) {
                sum += area;
            }
        }
        return sum;
    }

    public boolean save(String filename) throws DrawingException {
        if ((author == null || author.length() == 0) && (name == null || name.length() == 0)) {
            throw new DrawingException("The drawing is missing author and name");
        }
        
        if (author == null || author.length() == 0) {
            throw new DrawingException("The drawing is missing author");
        }

        if (name == null || name.length() == 0) {
            throw new DrawingException("The drawing is missing name");
        }

        return true;
    }

    @Override
    public void draw() {
        System.out.println("A drawing by " + author + " called " + name);
        for (Shape s : shapes) {
            System.out.println(s.toString());
        }
    }

    @Override
    public String toString() {
        if (getTotalCircumference() == 0 || getTotalArea() == 0) {
            return "Drawing[name=" + name + "; author=" + author + "; size=" + shapes.size()
                    + "; circumference=0.0; area=0.0 ;]";
        } else {
            return "Drawing[name=" + name + "; author=" + author + "; size=" + shapes.size() + "; circumference="
                    + getTotalCircumference() + "; area=" + getTotalArea() + ";]";
        }
    }

    @Override
    public void draw(Graphics g) {
        for (Shape shape : shapes) {
            System.out.println(shape);
            shape.draw(g);
        }

    }

}
