package se.miun.mani1912.dt187g.jpaint;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.*;

/**
 * Create a Circle object. This class also contains methods to get data in the
 * Circle such as radius, area and circumference. Inherits from the Shape class.
 * 
 * @author mani1912
 * @version 1.0
 */

public class Circle extends Shape {
    final static double PI = 3.14;

    public Circle(double x, double y, String color) {
        this(new Point(x, y), color);
    }

    public Circle(Point p, String color) {
        super(p, color);
    }

    public double getRadius() {
        if (hasEndpoint()) {
            // Pythagorean theorem
            double lengthX = Math.abs(points.get(1).getX() - points.get(0).getX());
            double lengthY = Math.abs(points.get(1).getY() - points.get(0).getY());
            return Math.hypot(lengthX, lengthY);
        } else {
            return -1;
        }
    }

    @Override
    public void draw() {
        // TODO Auto-generated method stub
        System.out.println(toString());

    }

    @Override
    public void draw(Graphics g) {
        // Uses Graphics2D for anti-alialising
        Graphics2D g2 = (Graphics2D)g;
        g2.drawOval((int)points.get(0).getX(), (int)points.get(0).getY(), (int)getRadius(), (int)getRadius());
        g2.setColor(Color.decode(getColor()));
        g2.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        g2.fillOval((int)points.get(0).getX(), (int)points.get(0).getY(), (int)getRadius(), (int)getRadius());

    }

    @Override
    public double getCircumference() {
        if (hasEndpoint()) {
            System.out.println("Radius is " + getRadius());
            return 2.0 * PI * getRadius();
        } else {
            return -1;
        }
    }

    @Override
    public double getArea() {
        if (hasEndpoint()) {
            return PI * getRadius() * getRadius();
        } else {
            return -1;
        }
    }

    @Override
    public boolean hasEndpoint() {
        if (points.size() > 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        if (hasEndpoint()) {
            return "Circle[start=" + points.get(0) + "; end=" + points.get(1) + "; radius=" + getRadius() + "; color="
                    + color + ";]";
        } else {
            return "Circle[start=" + points.get(0) + "; end=N/A; radius=N/A; color=" + color + ";]";
        }
    }

}
