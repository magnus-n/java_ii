package se.miun.mani1912.dt187g.jpaint;

/**
 * Test class for Circle. Create a new Circle, add the endpoint and prints the toString.
 * 
 * @author mani1912
 * @version 1.0
 */

public class TestCircle {
    public static void main(String[] args) {
        Circle circle = new Circle(1, 2, "blue");
        circle.addPoint(3, 4);
        System.out.println(circle.toString());
    }   
}
