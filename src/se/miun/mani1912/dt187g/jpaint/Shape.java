package se.miun.mani1912.dt187g.jpaint;

import java.util.ArrayList;
import java.util.List;

/**
 * The Shape class is a superclass to Rectangle and Circle classes. Shape holds
 * the shapes color, starting and ending points. Implements the Drawable
 * interface.
 * 
 * @author mani1912
 * @version 1.0
 */

public abstract class Shape implements Drawable {
    protected String color;
    protected List<Point> points = new ArrayList<Point>();

    public Shape(double x, double y, String color) {
        this(new Point(x, y), color);
    }

    public Shape(Point p, String color) {
        this.color = color;
        this.points.add(p);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public abstract double getCircumference();

    public abstract double getArea();

    public abstract boolean hasEndpoint();

    public void addPoint(Point p) {
        // Add endpoint
        if (p != null) {
            this.points.add(1, p);
        }
    }

    public void addPoint(double x, double y) {
        // Add endpoint
        addPoint(new Point(x, y));
    }
}
