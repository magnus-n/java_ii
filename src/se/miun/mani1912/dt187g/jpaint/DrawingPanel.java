package se.miun.mani1912.dt187g.jpaint;

import javax.swing.JPanel;
import java.awt.*;

public class DrawingPanel extends JPanel {

    private Drawing drawing;    

    public DrawingPanel() {
        super();
        this.drawing = new Drawing();

        // Test drawing
        //setDrawing(createTestDrawing());

        super.repaint();
    }

    public DrawingPanel(Drawing drawing) {
        super();
        this.drawing = drawing;
        super.repaint();
    }

    public void setDrawing(Drawing drawing) {
        this.drawing = drawing;
        super.repaint();
    }

    public void addShapesFromDrawing(Drawing drawing) {
        // Add the shapes from the drawing argument to current shape in DrawingPanel
        for (Shape shape : drawing.shapes) {
            this.drawing.addShape(shape);
        }
    }

    public Drawing getDrawing() {
        return drawing;
    }

    @Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

        if (drawing != null) {
            drawing.draw(g); 
        }

        super.repaint();

	}

    public Drawing createTestDrawing() {
        // Create a test drawing
        Drawing testDrawing = new Drawing("Mona Lisa", "Da Vinci");

        // Green rectangle
        Rectangle testRectangle1 = new Rectangle(new Point(100, 100), "#00FF00");
        testRectangle1.addPoint(new Point(150, 150));
        testDrawing.addShape(testRectangle1);

        // Black rectangle
        Rectangle testRectangle2 = new Rectangle(new Point(200, 100), "#000000");
        testRectangle2.addPoint(new Point(250, 150));
        testDrawing.addShape(testRectangle2);

        // Red rectangle
        Rectangle testRectangle3 = new Rectangle(new Point(500, 200), "#FF0000");
        testRectangle3.addPoint(new Point(600, 300));
        testDrawing.addShape(testRectangle3);

        // Blue circle
        Circle testCircle1 = new Circle(new Point(300, 200), "#0000FF");
        testCircle1.addPoint(new Point(400, 300));
        testDrawing.addShape(testCircle1);
        System.out.println("testDrawing " + testDrawing);

        // Yellow circle
        Circle testCircle2 = new Circle(new Point(250, 250), "#FFFF00");
        testCircle2.addPoint(new Point(300, 300));
        testDrawing.addShape(testCircle2);

        System.out.println("testDrawing " + testDrawing);
        return testDrawing;
    }





}
