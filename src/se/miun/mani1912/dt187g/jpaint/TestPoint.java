package se.miun.mani1912.dt187g.jpaint;

/**
 * Test class for the Point class. Create a new point and prints it's value
 * to the console.
 * 
 * @author mani1912
 * @version 1.0
 */

public class TestPoint {
    public static void main(String[] args) {
        Point point = new Point(1, 2);
        System.out.println(point.toString());
    }
}
