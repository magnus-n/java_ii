package se.miun.mani1912.dt187g.jpaint;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Create a Rectangle object. This class also contains methods to get data in
 * the Rectangle such as width, height, area and circumference. Inherits from
 * the Shape class.
 * 
 * @author mani1912
 * @version 1.0
 */

public class Rectangle extends Shape {

    public Rectangle(double x, double y, String color) {
        this(new Point(x, y), color);
    }

    public Rectangle(Point p, String color) {
        super(p, color);
    }

    public double getWidth() {
        if (hasEndpoint()) {
            return Math.abs(points.get(1).getX() - points.get(0).getX());
        } else {
            return -1;
        }
    }

    public double getHeight() {
        if (hasEndpoint()) {
            return Math.abs(points.get(1).getY() - points.get(0).getY());
        } else {
            return -1;
        }
    }

    @Override
    public void draw() {
        // TODO Auto-generated method stub
        System.out.println(toString());
    }

    @Override
    public void draw(Graphics g) {;
        g.drawRect((int)points.get(0).getX(), (int)points.get(0).getY(), (int)getWidth(), (int)getHeight());
        g.setColor(Color.decode(getColor()));
        g.fillRect((int)points.get(0).getX(), (int)points.get(0).getY(), (int)getWidth(), (int)getHeight());

    }

    @Override
    public double getCircumference() {
        if (hasEndpoint()) {
            return getWidth() * 2.0 + getHeight() * 2.0;
        } else {
            return -1;
        }
    }

    @Override
    public double getArea() {
        if (hasEndpoint()) {
            return getWidth() * getHeight();
        } else {
            return -1;
        }
    }

    @Override
    public boolean hasEndpoint() {
        if (points.size() > 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        if (hasEndpoint()) {
            return "Rectangle[start=" + points.get(0).toString() + ";end=" + points.get(1).toString() + ";width="
                    + getWidth() + ";height=" + getHeight() + ";color=" + color + "]";
        } else {
            return "Rectangle[start=" + points.get(0).toString() + ";end=N/A ;width=N/A ;height=N/A ;color=" + color
                    + "]";
        }
    }
}
